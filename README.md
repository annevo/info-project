# Getting Started
- Sign in or create a new [Gitlab Account](https://gitlab.com/users/sign_in)
- Request access to the [Annevo Gitlab group](https://gitlab.com/annevo)
- As soon as you have been granted access you should be able to create a new sub-group for your projects. A sub-group can hold any number of projects.
- Create a new empty project in your subgroup and push any existing git repository you might have.

## Auto deploy your application
- Copy the contents of [`annevo/info-project/templates/`](https://gitlab.com/annevo/info-project/-/tree/main/templates) to the root of your project.
- Edit the example values in the `Dockerfile`, `.dockerignore` and `.gitlab/auto-deploy-values.yaml` to your needs.
- The name of your sub-group will be used in combination with your project name to create a unique public URL for your application. 

Now every time you commit to your repository a new environment should be built and deployed automatically.


## Customization
Looking to customize or use environment variables? Check out [Additional autodeploy configurations](https://gitlab.com/annevo/info-project/-/wikis/Additional-Autodeploy-Configurations)


## Wiki
https://gitlab.com/annevo/info-project/-/wikis/home
